import 'package:flutter/material.dart';
import 'package:giphy_finder/ui/home.dart';

main() {
  runApp(MaterialApp(
    home: HomePage(),
    debugShowCheckedModeBanner: false,
    theme: ThemeData(hintColor: Colors.white),
  ));
}