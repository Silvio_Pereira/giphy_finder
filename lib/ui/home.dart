import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:transparent_image/transparent_image.dart';

import 'gifPage.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String? _search;

  int _offset = 0;

  _getGifs() async {
    http.Response response;
    _search = (_search != null ? _search : "");
    if (_search == "") {
      response = await http.get(Uri.parse("https://api.giphy.com/v1/gifs/trending?api_key=dS1uMLdSeKW0LsJ58ulzELSpRTDbUMKW&limit=20&rating=g"));
    } else {
      response = await http.get(Uri.parse("https://api.giphy.com/v1/gifs/search?api_key=dS1uMLdSeKW0LsJ58ulzELSpRTDbUMKW&q=$_search&limit=19&offset=$_offset&rating=g&lang=en"));
    }
    return json.decode(response.body);
  }

  int getCount(List data){
    _search = (_search != null ? _search : "");
    if(_search != ""){
      return data.length + 1;
    }else{
      return data.length;
    }
  }

  @override
  void initState() {
    super.initState();

    _getGifs().then((map) {
      print(map);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Image.network(
            "https://developers.giphy.com/branch/master/static/header-logo-8974b8ae658f704a5b48a2d039b8ad93.gif"),
        centerTitle: true,
      ),
      backgroundColor: Colors.black,
      body: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(10.0),
            child: TextField(
              onSubmitted: (a) {
                setState(() {
                  _search = a;
                  _offset = 0;
                });
              },
              decoration: InputDecoration(
                labelText: "Search",
                labelStyle: TextStyle(color: Colors.white),
                border: OutlineInputBorder(),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.white,
                    width: 0.0,
                  ),
                ),
              ),
              style: TextStyle(color: Colors.white, fontSize: 18.0),
              textAlign: TextAlign.center,
            ),
          ),
          Expanded(
              child: FutureBuilder(
                  future: _getGifs(),
                  builder: (context, snapshot) {
                    switch (snapshot.connectionState) {
                      case ConnectionState.waiting:

                      case ConnectionState.none:
                        return Container(
                            width: 200.0,
                            height: 200.0,
                            alignment: Alignment.center,
                            child: CircularProgressIndicator(
                              valueColor:
                              AlwaysStoppedAnimation<Color>(Colors.white),
                              strokeWidth: 5.0,
                            ));
                      default:
                        if (snapshot.hasError)
                          return Container();
                        else
                          return _createGifTable(context, snapshot);
                    }
                  }))
        ],
      ),
    );
  }

  Widget _createGifTable(BuildContext context, AsyncSnapshot snapshot) {
    return GridView.builder(
        padding: EdgeInsets.all(10.0),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, crossAxisSpacing: 10.0, mainAxisSpacing: 10.0),
        itemCount: getCount(snapshot.data["data"]),
        itemBuilder: (context, index) {
          _search = (_search != null ? _search : "");
          if(_search == "" || index < snapshot.data["data"].length){
            return GestureDetector(
              child: FadeInImage.memoryNetwork(
                placeholder: kTransparentImage,
                image: snapshot.data["data"][index]["images"]["fixed_height"]["url"],
                height: 300.0,
                fit: BoxFit.cover,
              ),
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (_) => GifPage(snapshot.data["data"][index])));
              },
              onLongPress: () async{
                var request = await HttpClient().getUrl(Uri.parse(snapshot.data["data"][index]["images"]["fixed_height"]["url"]));
                var response = await request.close();
                Uint8List bytes = await consolidateHttpClientResponseBytes(response);
                await Share.file(snapshot.data["data"][index]['title'], snapshot.data["data"][index]['title'] + ".gif", bytes, 'image/gif');
              },
            );
          }else{
            return Container(
              child: GestureDetector(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.add,
                      color: Colors.white,
                      size: 70,
                    ),
                    Text("Load More...",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 22
                    ),)
                  ],
                ),
                onTap: (){
                  setState(() {
                    _offset += 19;
                  });
                },
              ),
            );
          }
        });
  }
}