import 'dart:io';
import 'dart:typed_data';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class GifPage extends StatelessWidget {

  final Map gifData;

  GifPage(this.gifData);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(gifData['title']),
        backgroundColor: Colors.black,
        actions: [
          IconButton(onPressed: () async{
            var request = await HttpClient().getUrl(Uri.parse(gifData["images"]["fixed_height"]["url"]));
            var response = await request.close();
            Uint8List bytes = await consolidateHttpClientResponseBytes(response);
            await Share.file(gifData['title'], gifData['title'] + ".gif", bytes, 'image/gif');
          }, icon: Icon(Icons.share))
        ],
      ),
      backgroundColor: Colors.black,
      body: Center(
        child: Image.network(gifData['images']['fixed_height']['url']),
      ),
    );
  }
}
